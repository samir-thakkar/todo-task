import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AddTodoComponent} from './add-todo/add-todo.component';
import {DetailTodoComponent} from './detail-todo/detail-todo.component';

const routes: Routes = [
  {path: 'dashboard', component: DashboardComponent},
  {path: 'add', component: AddTodoComponent},
  {path: 'detail/:id', component: DetailTodoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
